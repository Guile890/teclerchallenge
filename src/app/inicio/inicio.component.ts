import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
export interface CiudadesElement {
  ciudad: string;
  city: string;  
}
export interface InfoAutos {
  vehiculo: string;
  nombre: string; 
  precio: number; 
}
export interface Alquiler {
  ciudad: number,
  tipo: string,
  autos: number,
  horas: number
}
export interface Estadistica {
  ciudad: number,
  name: string,
  total: number
}



@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  public config = new MatSnackBarConfig();
  public form: FormGroup;
  Ciudades: CiudadesElement[] = [
    {ciudad: '1', city: 'Guadalajara'},
    {ciudad: '2', city: 'Tijuana'},
    {ciudad: '3', city: 'Culiacán'},
    {ciudad: '4', city: 'Toluca'},
    {ciudad: '5', city: 'Mérida'},
  ];
  Info: InfoAutos[] = [
    {vehiculo: '1', nombre: 'bicicleta', precio: 4},
    {vehiculo: '2', nombre: 'moto', precio: 6},
    {vehiculo: '3', nombre: 'cuatrimoto', precio: 15},
    {vehiculo: '4', nombre: 'carro de golf', precio: 18},
  ];

  Alquileres : Array<Alquiler> = [];
  Estadisticas : Array<Estadistica> = [];
  public displayedColumns: string[] = ['ciudad', 'nombre'];
  dataSource;
  

  public displayedColumnsTwo: string[]= ['vehiculo','nombre','precio'];
  dataSourceAutos;

  public displayedColumnsThree: string[]= ['ciudad','tipo','autos','horas'];
  dataSourceAlquiler;

  public displayedColumnsFour: string[]= ['ciudad','nombreCiudad','total'];
  dataSourceResume;

  constructor(
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
    private changeDetectorRefs: ChangeDetectorRef
  ) { 
    this.dataSource = new MatTableDataSource;
    this.dataSourceAutos = new MatTableDataSource;
    this.dataSourceAlquiler = new MatTableDataSource;
    this.dataSourceResume = new MatTableDataSource;
  }

  ngOnInit(): void {
    this.dataSource = this.Ciudades;
    this.dataSourceAutos = this.Info;
    this.form = this.formBuilder.group({
      ciudad : ['',Validators.required],
      tipo : ['',Validators.required],
      autos : ['',Validators.required],
      horas : ['',Validators.required]
    });
  }
  public registrar(){
    console.log('valor del form',this.form.value);
    if(this.form.status == 'INVALID'){
      this.config.duration = 4000;
        this.config.panelClass = ['errorSnack'];
        this.matSnackBar.open("Ingrese los valores obligatorios","",this.config);
    }else{      
      this.Alquileres.push(
        {ciudad: this.form.value.ciudad,
        tipo: this.form.value.tipo,
        autos: this.form.value.autos,
        horas: this.form.value.horas})       
      this.dataSourceAlquiler = this.Alquileres;
      let tarifa;
      if(this.form.value.tipo == 'moto'){
        tarifa = 6;
      }
      else if(this.form.value.tipo === 'bicicleta'){
        tarifa = 4;
      }
      else if(this.form.value.tipo === 'cuatrimoto'){
        tarifa = 15;
      }
      else if(this.form.value.tipo === 'carro de golf'){
        tarifa = 18;
      }
      const sumatotal = (this.form.value.autos* this.form.value.horas)*tarifa;
      console.log('valor suma total', sumatotal);
      console.log('valor tarifa', tarifa);
      this.Estadisticas.push(
        {
         ciudad:this.form.value.ciudad,
         name:this.form.value.ciudad,
         total: sumatotal
        }
      )
      this.dataSourceResume = this.Estadisticas;
      console.log('valor de dataresume', this.dataSourceResume)
      console.log('valor de data', this.dataSourceAlquiler)
    }
    this.refrescar();
  }
  public refrescar(){
    this.changeDetectorRefs.detectChanges();
    
  }

}
